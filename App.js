import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import HomeScreen from './screens/HomeScreen'
import ProfileScreen from './screens/ProfileScreen'
import Attendence from './screens/Attendence'
import Audit from './screens/Audit'
import Merchandising from './screens/Merchandising';
import VendorMangement from './screens/VendorMangement';
import MysteryAudit from './screens/MysterryAudit'
import Settings from './screens/Settings'
import Maps from './globalComponents/Maps'
import DetailsScreen from './screens/Detailes'
import CreateForm from './screens/CreateForm';



const Stack = createNativeStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
<Stack.Navigator screenOptions={{
    headerShown: false
  }} >


        <Stack.Screen name="Home" component={HomeScreen}  />
        <Stack.Screen name="Details" component={ProfileScreen} />
        <Stack.Screen name="Attendence" component={Attendence} />
        <Stack.Screen name="Audit" component={Audit} />
        <Stack.Screen name="Merchandising" component={Merchandising} />
        <Stack.Screen name="VendorMangement" component={VendorMangement} />
        <Stack.Screen name="MysteryAudit" component={MysteryAudit} />
        <Stack.Screen name="Settings" component={Settings} />
        <Stack.Screen name="Maps" component={Maps} />
        <Stack.Screen name="DetailsScreen" component={DetailsScreen} />
        <Stack.Screen name="CreateForm" component={CreateForm} />
        
      </Stack.Navigator>
      

    </NavigationContainer>
  );
};

export default App;