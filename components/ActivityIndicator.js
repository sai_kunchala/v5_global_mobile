/**
 * @format
 */
import React from 'react';

import {
  StyleSheet,
  View,
  Modal,
  ActivityIndicator as AI,
  Text,
} from 'react-native';
import PropTypes from 'prop-types';

const ActivityIndicator = ({
  animating = false,
  color = 'blue',
  size = 'large',
  opacity = 0.4,
}: {
  animating: boolean;
  color?: string;
  size?: number | 'large' | 'small' | undefined;
  opacity?: number;
}): JSX.Element => {
  return (
    <Modal
      transparent
      animationType={'none'}
      visible={animating}
      onRequestClose={() => null}>
      <View
        style={[
          styles.modalBackground,
          { backgroundColor: `rgba(0,0,0,${opacity})` },
        ]}>
        <View style={styles.activityIndicatorWrapper}>
          <AI animating={animating} color={color} size={size} />
          <Text style={styles.loadingText}>Loading...</Text>
        </View>
      </View>
    </Modal>
  );
};

ActivityIndicator.propTypes = {
  animating: PropTypes.bool.isRequired,
  color: PropTypes.string,
  size: PropTypes.string,
  opacity: (props, propName, componentName) => {
    if (props[propName] < 0 || props[propName] > 1) {
      return new Error('Opacity prop value out of range');
    }
  },
};

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  activityIndicatorWrapper: {
    backgroundColor: 'white',
    height: 100,
    width: 100,
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  loadingText: {
    color: 'black',
  },
});

export default ActivityIndicator;
