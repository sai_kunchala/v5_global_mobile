import React from 'react';
import PropTypes from 'prop-types';
import { SearchableDropDownPicker, HelperText } from '@app/components/library';
import { useController, useFormContext } from 'react-hook-form';

const SearchableDropDownPickerWithError = React.forwardRef(
  ({ error, ...textInputProps }, ref) => {
    const isError = Boolean(error);

    return (
      <>
        <SearchableDropDownPicker
          ref={ref}
          error={isError}
          {...textInputProps}
        />
        {isError && (
          <HelperText type="error" visible={isError}>
            {error}
          </HelperText>
        )}
      </>
    );
  },
);

const FormSearchableDropDownPicker = React.forwardRef(
  (props, ref): JSX.Element => {
    const { name, rules, defaultValue, ...inputProps } = props;

    const {
      control,
      formState: { errors },
    } = useFormContext();

    if (!control || !name) {
      const errorMessage = !name
        ? 'Form field must have a "name" prop!'
        : 'Form field must be a descendant of `FormProvider` as it uses `useFormContext`!';
      return (
        <SearchableDropDownPickerWithError
          {...inputProps}
          error={errorMessage}
          editable={false}
        />
      );
    }

    const { field } = useController({
      name,
      control,
      rules,
      defaultValue,
    });
    return (
      <SearchableDropDownPickerWithError
        ref={ref}
        {...inputProps}
        error={errors?.[name]?.message}
        setValue={field.onChange}
        onBlur={field.onBlur}
        value={field.value}
      />
    );
  },
);

FormSearchableDropDownPicker.propTypes = {
  name: PropTypes.string.isRequired,
  rules: PropTypes.object,
  defaultValue: PropTypes.any,
};

export default FormSearchableDropDownPicker;
