import React from 'react';
import PropTypes from 'prop-types';
import { TextInput, HelperText } from '@app/components/library';
import { useController, useFormContext } from 'react-hook-form';

const TextInputWithError = React.forwardRef(
  ({ error, ...textInputProps }, ref) => {
    const isError = Boolean(error);

    return (
      <>
        <TextInput
          mode="outlined"
          ref={ref}
          error={isError}
          autoCapitalize="none"
          autoCorrect={false}
          {...textInputProps}
          style={
            isError
              ? [textInputProps?.style, { marginBottom: 0 }]
              : textInputProps?.style
          }
        />
        {isError && (
          <HelperText
            style={[textInputProps?.style, { marginTop: 0, marginBottom: 0 }]}
            type="error"
            visible={isError}>
            {error}
          </HelperText>
        )}
      </>
    );
  },
);

const FormTextInput = React.forwardRef((props, ref): JSX.Element => {
  const { name, rules, defaultValue, ...inputProps } = props;

  const {
    control,
    formState: { errors },
  } = useFormContext();

  if (!control || !name) {
    const errorMessage = !name
      ? 'Form field must have a "name" prop!'
      : 'Form field must be a descendant of `FormProvider` as it uses `useFormContext`!';
    return (
      <TextInputWithError
        ref={ref}
        {...inputProps}
        error={errorMessage}
        editable={false}
      />
    );
  }

  const { field } = useController({
    name,
    control,
    rules,
    defaultValue,
  });
  return (
    <TextInputWithError
      ref={ref}
      {...inputProps}
      error={errors?.[name]?.message}
      onChangeText={field.onChange}
      onBlur={field.onBlur}
      value={field.value}
    />
  );
});

FormTextInput.propTypes = {
  name: PropTypes.string.isRequired,
  rules: PropTypes.object,
  defaultValue: PropTypes.any,
};

export default FormTextInput;
