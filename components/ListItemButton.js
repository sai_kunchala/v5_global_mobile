import React from 'react';
import {
  TouchableOpacity,
  Image,
  GestureResponderEvent,
  ImageSourcePropType,
  StyleSheet,
} from 'react-native';
import { List, Surface } from 'react-native-paper';

const ListItemButton = (props: {
  menuLeftImage: ImageSourcePropType;
  menuLeftImageSize: number;
  menuTitle: string;
  handleOnPress: ((event: GestureResponderEvent) => void) | undefined;
}): JSX.Element => {
  return (
    <TouchableOpacity activeOpacity={0.5} onPress={props?.handleOnPress}>
      <Surface style={styles.surface}>
        <List.Item
          title={props?.menuTitle}
          left={() => (
            <List.Icon
              style={{ height: props?.menuLeftImageSize - 10 }}
              icon={() => (
                <Image
                  source={props?.menuLeftImage}
                  style={{
                    width: props?.menuLeftImageSize,
                    height: props?.menuLeftImageSize,
                  }}
                />
              )}
            />
          )}
          right={() => (
            <List.Icon
              style={{ height: props?.menuLeftImageSize - 10 }}
              icon="chevron-right"
            />
          )}
        />
      </Surface>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  surface: {
    marginVertical: 8,
    marginHorizontal: 2,
    elevation: 4,
  },
});

export default ListItemButton;
