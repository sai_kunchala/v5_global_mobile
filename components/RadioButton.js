/**
 * @format
 */
import { useTheme } from '@react-navigation/native';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { View, Text, Alert, StyleSheet } from 'react-native';
import { IconButton, RadioButton as RB } from 'react-native-paper';

export const RadioButton = (props): JSX.Element => {
  const { colors } = useTheme();
  return <RB.Android {...props} color={colors.primary} />;
};

export const RadioButtonGroup = (props): JSX.Element => {
  const {
    type,
    groupLabel,
    firstValue,
    secondValue,
    firstLabel,
    secondLabel,
    value,
    onValueChange,
  } = props;
  const { t } = useTranslation();
  return (
    <RB.Group onValueChange={onValueChange} value={value}>
      <Text>{groupLabel}</Text>
      <View style={styles.centerInRow}>
        <View style={styles.centerInRow}>
          <RadioButton value={firstValue} disabled={props?.disabled} />
          <Text>{firstLabel}</Text>
          {type === 'EUIN Declaration' && (
            <IconButton
              icon="information-outline"
              size={20}
              onPress={() =>
                Alert.alert('EUIN Declaration', t('euinDeclaration'), [
                  { text: 'OK' },
                ])
              }
            />
          )}
        </View>
        <View style={styles.centerInRow}>
          <RadioButton value={secondValue} disabled={props?.disabled} />
          <Text>{secondLabel}</Text>
        </View>
      </View>
    </RB.Group>
  );
};

const styles = StyleSheet.create({
  centerInRow: { flexDirection: 'row', alignItems: 'center' },
});
