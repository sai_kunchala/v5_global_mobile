/**
 * @format
 */
import { logMsg } from '@app/utils/analytics';
import React, { useState } from 'react';
import { StyleSheet, View } from 'react-native';
import DropDownPicker, {
  DropDownPickerProps,
} from 'react-native-dropdown-picker';
import { List, Text, useTheme } from 'react-native-paper';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

const SearchableDropDownPicker = (
  props: JSX.IntrinsicAttributes &
    DropDownPickerProps & { children?: React.ReactNode },
): JSX.Element => {
  const [open, setOpen] = useState(false);
  const { colors } = useTheme();
  return (
    <View style={props.style}>
      {props?.label && (
        <Text style={stylesWithProps(props).label}>{props?.label}</Text>
      )}
      <DropDownPicker
        translation={{
          PLACEHOLDER: `Select ${props?.label}`,
          SEARCH_PLACEHOLDER: 'Type to search/filter...',
          NOTHING_TO_SHOW: `No ${props?.label} list could be fetched`,
          SELECTED_ITEMS_COUNT_TEXT: `{count} ${props?.label} selected`,
        }}
        searchable={true}
        searchablePlaceholder="Search..."
        listMode="MODAL"
        flatListProps={{
          keyboardShouldPersistTaps: 'always',
          initialNumToRender: 15,
        }}
        itemSeparator={true}
        itemSeparatorStyle={{
          backgroundColor: 'lightgrey',
        }}
        selectedItemLabelStyle={{
          color: colors.primary,
        }}
        TickIconComponent={() => (
          <MaterialCommunityIcons
            name="check"
            color={colors.primary}
            size={20}
          />
        )}
        style={stylesWithProps(props).picker}
        disabledStyle={styles.disable}
        {...props}
        // placeholder={`Select ${props?.label}`}
        open={open}
        setOpen={setOpen}
        schema={props?.schema}
        value={props?.value}
        setValue={props?.setValue}
        items={props?.items}
        setItems={props?.setItems}
        onChangeItem={props?.onChangeItem}
      />
    </View>
  );
};

const stylesWithProps = (props: unknown) =>
  StyleSheet.create({
    picker: {
      height: 42,
      borderTopLeftRadius: 5,
      borderTopRightRadius: 5,
      borderBottomLeftRadius: 5,
      borderBottomRightRadius: 5,
      borderWidth: props?.error ? 2 : 1,
      borderColor: props?.error ? '#B00020' : 'grey',
    },
    label: {
      color: props?.error ? '#B00020' : 'black',
    },
  });

const styles = StyleSheet.create({
  disable: {
    opacity: 0.5,
  },
});

export default SearchableDropDownPicker;
