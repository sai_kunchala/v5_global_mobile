/**
 * @format
 */
import React from 'react';
import { Text, View } from 'react-native';
import { Switch as Sw, useTheme } from 'react-native-paper';

const Switch = (props): JSX.Element => {
  const { colors } = useTheme();
  return (
    <View
      style={[
        props?.style,
        {
          alignItems: 'center',
          flexDirection: 'row',
        },
      ]}>
      <Text style={{ marginLeft: 10 }}>{props?.label}</Text>
      <Sw
        color={colors.primary}
        value={props?.value}
        onValueChange={props?.onValueChange}
      />
    </View>
  );
};
export default Switch;
