/**
 * @format
 */
import React, { useRef } from 'react';
import { StyleSheet, View } from 'react-native';
import { Text, TextInput as TI } from 'react-native-paper';

const TextInput = React.forwardRef((props, ref): JSX.Element => {
  return (
    <>
      {props?.label && (
        <Text style={[props?.style, stylesWithProps(props).label]}>
          {props?.label}
        </Text>
      )}
      <TI
        ref={ref}
        dense
        {...props}
        style={[props?.style, { marginTop: 0 }]}
        label=""
      />
    </>
  );
});

export const TextInputAffix = TI.Affix;
export const TextInputIcon = TI.Icon;

const stylesWithProps = (props: unknown) =>
  StyleSheet.create({
    label: {
      color: props?.error ? '#B00020' : 'black',
      marginBottom: 0,
    },
  });
export default TextInput;
