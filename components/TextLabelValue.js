/**
 * @format
 */
import React from 'react';
import { View } from 'react-native';
import { Text } from 'react-native-paper';

const TextLabelValue = (props): JSX.Element => {
  return (
    <View style={[props?.style, { flexDirection: 'column' }]}>
      <Text style={{ color: 'grey' }}>{props?.label}</Text>
      <Text>{props?.value}</Text>
    </View>
  );
};
export default TextLabelValue;
