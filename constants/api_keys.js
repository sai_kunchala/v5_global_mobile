/**
 * @format
 */

export const API_RESPONSE_KEYS = {
  USER_ID: 'USER_ID',
  USER_NAME: 'UserName',
  USER_EMAIL: 'Login_MailID',
  TABLE4: {
    "subModuleName":"", "moduleId":"", 
    "roles":["roleId","roleId2"] 
    "name":"screen Name", 
    "submoduleId":"subModuleId",
     "clientId":"clientId", 
     "screen":[]
  },
  TABLE6: {
    FUND: 'Fund',
    FUND_DESC: 'FundDesc',
    CURRENT_VALUE: 'CurrentValue',
    GAIN_VAL: 'GainVal',
    GAIN: 'Gain',
    COST_VALUE: 'CostValue',
    PERCENTAGE: 'Percentage',
    PAN: 'PAN',
    TODAY_GAIN: 'todaygain',
    TOT_PERCENTAGE: 'totpercent',
    GAIN_PERCENTAGE: 'GainPercent',
  },
  TABLE12: {
    INDEX: 'id',
    PAN: 'PAN',
    FLAG: 'flag',
    INV_NAME: 'invname',
    KYC: 'KYC',
    MINOR: 'minor',
    RED_LIMIT_FLAG: 'red_limit_flag',
  },
  TABLE13: {
    FUND: 'Fund',
    FUND_DESC: 'FundDesc',
    SCHEME_CLASS: 'SchemeClass',
    CURRENT_VALUE: 'CurrentValue',
    COST_VALUE: 'CostValue',
    GAIN_VALUE: 'GainValue',
    GAIN_PERCENT: 'GainPercent',
    PAN: 'pan',
    IMAGE_PATH: 'image_path',
    UNITS: 'units',
  },
  NEW_PURCHASE: {
    AMC_CODE: 'amc_code',
    AMC_NAME: 'amc_name',
    AADHAR: 'Aadhar',
    FM_SUBCATEGORY: 'fm_subcategory',
    SCHEME_PLAN: 'Scheme_Plan',
    SCHEME_DESC: 'Scheme_Desc',
    MIN_AMOUNT: 'MinAmt',
    PLAN_DESC: 'Plan_Desc',
    PLANDESC: 'PlanDesc',
    COMBO_FLAG: 'combo_flag',
    COMBO_LABEL: 'combo_flag',
    COMBO_DISCLAIMER: 'combo_disclaimer',
    TAX_SAVER_FLAG: 'TaxSaverFlag',
    MULTIPLES: 'multiples',
  },
  NAV_GRAPH: {
    AMC_CODE: 'amc_code',
    AMC_NAME: 'amc_name',
    AADHAR: 'Aadhar',
    FM_SUBCATEGORY: 'fm_subcategory',
    SCHEME_PLAN: 'Scheme_Plan',
    SCHEME_DESC: 'Scheme_Desc',

    PLAN_DESC: 'Plan_Desc',
    PLANDESC: 'PlanDesc',
    COMBO_FLAG: 'combo_flag',
    COMBO_LABEL: 'combo_flag',
    COMBO_DISCLAIMER: 'combo_disclaimer',
  },
IDWC :{
  AMC_CODE: 'amc_code',
  AMC_NAME: 'amc_name',
  AADHAR: 'Aadhar',
  FM_SUBCATEGORY: 'fm_subcategory',
  SCHEME_PLAN: 'Scheme_Plan',
  SCHEME_DESC: 'Scheme_Desc',

  PLAN_DESC: 'Plan_Desc',
  PLANDESC: 'PlanDesc',
  COMBO_FLAG: 'combo_flag',
  COMBO_LABEL: 'combo_flag',
  COMBO_DISCLAIMER: 'combo_disclaimer',
},








};

export const API_REQUEST_KEYS = {
  LOGIN: {
    REQ_BY: 'ReqBy',
    USER_NAME: 'username',
    PASSWORD: 'Password',
    TOKEN_ID: 'tokenID',
    LOGIN_MODE: 'loginMode',
  },
  FORGOT: {
    REQ_BY: 'ReqBy',
    EMAIL_ID: 'Emailid',
  },
  NEW_PURCHASE: {
    OPTION: 'opt',
    OPTION_VALUES: { A: 'A', AT: 'AT', S: 'S', NA: 'NA' },
    FUND: 'fund',
    SCHEME_TYPE: 'schemetype',
    PLAN_TYPE: 'plntype',
    PLAN_TYPE_VALUES: {
      DISTRIBUTOR: 'REGULAR',
      DIRECT: 'DIRECT',
    },
  },
  NAV_GRAPH: {
    PLAN: 'Plan',
    SCHEME: 'Scheme',
    FUND: 'Fund',
    FLAG: 'flag',
  },
  IDWC:{
    IHNO :'Ihno',
    IMEI :'IMEI'

  }
};
