export const ASYNC_STORAGE_CONSTANTS = {
  IS_FIRST_TIME_LAUNCH: 'is_first_time_launch',
  USER_ID: 'user_id',
  PASSWORD: 'password',
  REMEMBER_ME: 'remember_me',
  LOGIN_PIN: 'login_pin',
  LOGIN_PATTERN: 'login_pattern',
  FAILED_LOGIN_TIME: 'failed_login_time',
  IS_LOGGED_IN: 'is_logged_In',
  IS_FIRST_TIME_LOGIN: 'is_first_time_login',
  SKIP_LOGIN: 'skip_login',
};
