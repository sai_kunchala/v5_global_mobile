export const menuImages = {
  portfolioImg: require('../assets/menu_portfolio.png'),
  transactionHistoryImg: require('../assets/menu_transaction_history.png'),
  transactionImg: require('../assets/menu_transact.png'),
  transactionFamilyImg: require('../assets/menu_family_trans.png'),
  navImg: require('../assets/menu_historic_nv.png'),
  minorTransactionImg: require('../assets/menu_minor_trans.png'),
  dividendImg: require('../assets/menu_dividend.png'),
  statementsImg: require('../assets/menu_statements.png'),
  sipCalendarImg: require('../assets/sip_calendar.png'),
  familyFolioImg: require('../assets/menu_associate_folio.png'),
  cancelImg: require('../assets/menu_cancel.png'),
  enachImg: require('../assets/menu_eotm.png'),
  notificationsImg: require('../assets/menu_notifications.png'),
  whatsappImg: require('../assets/menu_whatsapp.png'),
  profileImg: require('../assets/menu_profile_details.png'),
  logoutImg: require('../assets/menu_logout.png'),
};

export const transactionImages = {
  addPurchaseImg: require('../assets/addpurchase.png'),
  newPurchaseImg: require('../assets/schedule_newpurchase.png'),
  redRoboImg: require('../assets/red_robo.png'),
  redemptionImg: require('../assets/redem.png'),
  instaCashRedemptionImg: require('../assets/instacash.png'),
  switchImg: require('../assets/switch_icon.png'),
  sipImg: require('../assets/sip1.png'),
  swpImg: require('../assets/swp.png'),
  stpImg: require('../assets/stp.png'),
};

export const commonImages = {
  closeImg: require('../assets/close.png'),
  logoutImg: require('../assets/menu_logout.png'),
};

export const paymentModeImages = {
  netBankingImg: require('@app/assets/netbanking.png'),
  kotmImg: require('@app/assets/kotm.png'),
  debitCardImg: require('@app/assets/debit_crad.png'),
  upiImg: require('@app/assets/upi.png'),
};
