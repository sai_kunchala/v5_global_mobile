import { useContext } from 'react';
import {
  AuthContext,
  AuthContextInterface,
} from '@app/context_providers/auth_context';

export const useAuth = (): AuthContextInterface => {
  const context = useContext(AuthContext);
  if (context === undefined) {
    throw new Error('AuthContext must be used within a AuthProvider');
  }
  return context;
};
